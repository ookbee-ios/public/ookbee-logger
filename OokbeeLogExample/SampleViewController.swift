//
//  SampleViewController.swift
//  OokbeeLogExample
//
//  Created by Peerasak Unsakon on 12/7/2564 BE.
//

import UIKit
import SnapKit

class SampleViewController: UITableViewController {
    
    let cellId = "LogCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SampleTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? SampleTableViewCell {
            return cell
        }
        fatalError("could not dequeueReusableCell")
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
