//
//  ViewController.swift
//  OokbeeLogExample
//
//  Created by Peerasak Unsakon on 8/6/2564 BE.
//

import UIKit
import OokbeeLog

class ViewController: UIViewController, OokbeeLogableViewController {
    
    private var counter: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        OokbeeLog.setMaximumRecord(to: 40)
        
        OokbeeLog.info(text: "Hello", save: true)
        OokbeeLog.debug(text: "World", save: true)
        OokbeeLog.warning(text: "แปลกๆแล้วนะ", save: true)
        OokbeeLog.error(text: "Error แล้ว", save: true)
        
        loadJson(urlString: "https://gist.githubusercontent.com/clonezer/be17cc2b45a808f3eb15bf5cf6071e90/raasdasd;lkzxc")
        loadJson(urlString: "https://gist.githubusercontent.com/clonezer/be17cc2b45a808f3eb15bf5cf6071e90/raw/2dfc5ea41aa065a75dc7d02bdb5474800a73ef16/response.json")
    }
    
    func loadJson(urlString: String) {
        let url = URL(string: urlString)!
        let request = URLRequest(url: url)
        OokbeeLog.request(request)
        URLSession.shared.loadData(using: request) { (data, response, error) in
            OokbeeLog.response(data, response: response, error: error)
        }
    }
    
    @IBAction func addLogButtonTapped(_ sender: Any) {
        OokbeeLog.debug(text: "Check: \(String(self.counter))", feature: "counter", save: true)
        self.counter = self.counter + 1
    }
    
    @IBAction func showButtonTapped(_ sender: Any) {
        self.showLogs()
    }
    
}

protocol NetworkLoader {
    func loadData(using request: URLRequest, with completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: NetworkLoader {
    
    func loadData(using request: URLRequest, with completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.dataTask(with: request, completionHandler: completion).resume()
    }
    
}
