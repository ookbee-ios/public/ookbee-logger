//
//  SampleTableViewCell.swift
//  OokbeeLogExample
//
//  Created by Peerasak Unsakon on 12/7/2564 BE.
//

import UIKit
import SnapKit
import OokbeeLog

class SampleTableViewCell: UITableViewCell {
    
    lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .white
        label.textAlignment = .center
        label.backgroundColor = .red
        label.numberOfLines = 1
        return label
    }()
    
    lazy var featureLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var timeStampLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(typeLabel)
        addSubview(messageLabel)
        
        typeLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = typeLabel.superview else { return }
            make.top.equalTo(superview).offset(8)
            make.leading.equalTo(superview).offset(8)
            make.width.lessThanOrEqualTo(200)
            make.width.greaterThanOrEqualTo(80)
        }
        
        messageLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = messageLabel.superview else { return }
            make.top.equalTo(typeLabel.snp.bottom).offset(8)
            make.trailing.equalTo(superview).offset(-8)
            make.bottom.equalTo(superview).offset(-8)
            make.leading.equalTo(typeLabel)
        }
    }
    
    public func configure(logItem: OokbeeLogDescriptor) {
//        self.typeLabel.text = logItem.type.toTitle
//        self.typeLabel.backgroundColor = logItem.type.toColor
//        self.featureLabel.text = logItem.feature
//        self.messageLabel.text = logItem.message
//
//        let formatter = DateFormatter()
//        formatter.locale = Locale(identifier: "th_TH")
//        formatter.dateFormat = "d MMM yy HH:mm:ss น."
//
//        self.timeStampLabel.text = formatter.string(from: logItem.timestamp)
    }
    
    
    
}
