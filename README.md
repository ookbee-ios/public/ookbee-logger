# Ookbee Log

A log collector for Ookbee app

## Installation

Install `OokbeeLog` with cocoapods

```ruby
source 'https://gitlab.com/ookbee-ios/public/ookbee-podspec.git'

pod "OokbeeLog"
```

## Usage/Examples

Activity Log

```swift
import OokbeeLog

OokbeeLog.debug(text: "debug")
OokbeeLog.info(text: "info")
OokbeeLog.warning(text: "warning")
OokbeeLog.error(text: "error")

// for networking
OokbeeLog.request(request)
OokbeeLog.response(data: data, response: response, error: error)
```

Present Log collector view controller

```swift
import OokbeeLog

class ViewController: UIViewController, OokbeeLogableViewController {

    @IBAction func presentLogsButtonTapped(_ sender: Any) {
        self.showLogs()
    }

}
```
