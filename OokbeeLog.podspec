#
#  Be sure to run `pod spec lint OokbeeLog.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "OokbeeLog"
  spec.version      = "0.1.11"
  spec.summary      = "A log collector for Ookbee app."

  spec.description  = <<-DESC
  A log collector for Ookbee app
                   DESC

  spec.homepage     = "https://gitlab.com/ookbee-ios/public/ookbee-logger"

  spec.license      = { :type => "MIT", :file => "LICENSE" }

  spec.author             = { "Peerasak Unsakon" => "peerasak@ookbee.com" }

  spec.ios.deployment_target = "10.0"
  spec.swift_version = "4.2"

  spec.source       = { :git => "https://gitlab.com/ookbee-ios/public/ookbee-logger.git", :tag => "OokbeeLog_#{spec.version}" }

  spec.source_files  = "OokbeeLog", "OokbeeLog/**/*.{h,m,swift}"

  spec.dependency "SnapKit", "~> 5.0.0"
  spec.dependency "Highlightr"

end
