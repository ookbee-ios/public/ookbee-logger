fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios push_update
```
fastlane ios push_update
```
Push update podspec
### ios push_patch_update
```
fastlane ios push_patch_update
```
Update patch version
### ios push_minor_update
```
fastlane ios push_minor_update
```
Update minor version
### ios push_major_update
```
fastlane ios push_major_update
```
Update major version

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
