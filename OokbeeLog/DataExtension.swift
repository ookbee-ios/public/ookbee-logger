//
//  DataExtension.swift
//  OokbeeLog
//
//  Created by Peerasak Unsakon on 15/7/2564 BE.
//

import Foundation

extension Data {

    var prettyPrintedJSONString: NSString? {
        
        guard
            let object = try? JSONSerialization.jsonObject(with: self, options: []),
            let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
            let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        else {
            return nil
        }

        return prettyPrintedString
    }
    
}
