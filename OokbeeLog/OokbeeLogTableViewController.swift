//
//  OokbeeLogTableViewController.swift
//  OokbeeLog
//
//  Created by Peerasak Unsakon on 12/7/2564 BE.
//

import UIKit

public class OokbeeLogTableViewController: UITableViewController {
    
    let cellId = "LogCell"
    let networkCellId = "NetworkLogCell"
    
    public var selectedFeature: String?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        title = "Ookbee Logs"
        tableView.register(OokbeeNetworkLogTableViewCell.self, forCellReuseIdentifier: networkCellId)
        tableView.register(OokbeeLogTableViewCell.self, forCellReuseIdentifier: cellId)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Clear All", style: .plain, target: self, action: #selector(clearLogsButtonTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "×", style: .plain, target: self, action: #selector(closeLogsButtonTapped))
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollToBottom()
    }
    
    @IBAction private func clearLogsButtonTapped() {
        OokbeeLog.clearLogs()
        tableView.reloadData()
    }
    
    @IBAction private func closeLogsButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func scrollToBottom() {
        
        DispatchQueue.main.async { [weak self] in
            guard
                let sections = self?.tableView.numberOfSections,
                let rows = self?.tableView.numberOfRows(inSection: 0),
                sections > 0,
                rows > 0
            else {
                return
            }
            
            let indexPath = IndexPath(row: rows - 1, section: 0)
            self?.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        
    }

    private func presentDetails(indexPath: IndexPath) {
        guard let logItem = OokbeeLog.logItem(at: indexPath, feature: self.selectedFeature) as? OokbeeNetworkLogItem else { return }
        let detailViewController: OokbeeNetworkLogDetailsViewController = OokbeeNetworkLogDetailsViewController()
        detailViewController.contentConfigure(logItem: logItem)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OokbeeLog.numberOfRows(feature: self.selectedFeature)
    }
    
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let logItem: OokbeeLogDescriptor = OokbeeLog.logItem(at: indexPath, feature: self.selectedFeature)
        
        let networkTypes: [OokbeeLog.LogType] = [.request, .response]
        
        if networkTypes.contains(logItem.type) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: networkCellId, for: indexPath) as? OokbeeNetworkLogTableViewCell else {
                fatalError("Could not dequeueReusableCell")
            }
            
            guard let networkLogItem = logItem as? OokbeeNetworkLogDescriptor else {
                fatalError("Could not create network log")
            }
            
            cell.configure(logItem: networkLogItem)
            
            return cell
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? OokbeeLogTableViewCell else {
                fatalError("Could not dequeueReusableCell")
            }
            
            cell.configure(logItem: logItem)
            
            return cell
        }
    
    }
    
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presentDetails(indexPath: indexPath)
    }
    
}

public protocol OokbeeLogableViewController {
    func showLogs()
}

extension OokbeeLogableViewController where Self: UIViewController {
    
    public func showLogs() {
        let logsViewController = OokbeeLogTableViewController()
        let navigationController = UINavigationController(rootViewController: logsViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
}
