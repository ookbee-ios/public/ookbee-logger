//
//  OokbeeLogTableViewCell.swift
//  OokbeeLog
//
//  Created by Peerasak Unsakon on 8/6/2564 BE.
//  Copyright © 2564 BE Jeerapon. All rights reserved.
//

import UIKit
import SnapKit

class OokbeeLogTableViewCell: UITableViewCell {
    
    lazy var typeLabel: OokbeeLogTypeLabel = {
        let label = OokbeeLogTypeLabel(withInsets: 8, 8, 8, 8)
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .black
        label.textAlignment = .center
        label.backgroundColor = .red
        label.numberOfLines = 1
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
        label.sizeToFit()
        return label
    }()
    
    lazy var featureLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var timeStampLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(typeLabel)
        addSubview(messageLabel)
        addSubview(timeStampLabel)
        addSubview(featureLabel)
        
        typeLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = typeLabel.superview else { return }
            make.top.equalTo(superview).offset(8)
            make.leading.equalTo(superview).offset(8)
            make.height.equalTo(20)
        }
        
        messageLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = messageLabel.superview else { return }
            make.top.equalTo(typeLabel.snp.bottom).offset(8)
            make.trailing.equalTo(superview).offset(-8)
            make.bottom.equalTo(superview).offset(-8)
            make.leading.equalTo(typeLabel).offset(8)
        }
        
        timeStampLabel.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(typeLabel.snp.right).offset(8)
            make.centerY.equalTo(typeLabel)
        }
        
        featureLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = featureLabel.superview else { return }
            make.trailing.equalTo(superview).offset(-8)
            make.centerY.equalTo(typeLabel)
        }
    }
    
    public func configure(logItem: OokbeeLogDescriptor) {
        self.typeLabel.text = logItem.type.toTitle
        self.typeLabel.backgroundColor = logItem.type.toColor
        
        self.featureLabel.text = logItem.feature
        
        self.messageLabel.text = logItem.message
        self.messageLabel.textColor = logItem.type.toMessageColor
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "d MMM yy - HH:mm:ss"
        
        self.timeStampLabel.text = formatter.string(from: logItem.timestamp)
    }

}

class OokbeeNetworkLogTableViewCell: UITableViewCell {
    
    lazy var typeLabel: OokbeeLogTypeLabel = {
        let label = OokbeeLogTypeLabel(withInsets: 8, 8, 8, 8)
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .black
        label.textAlignment = .center
        label.backgroundColor = .red
        label.numberOfLines = 1
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
        label.sizeToFit()
        return label
    }()
    
    lazy var urlLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .gray
        label.textAlignment = .left
        label.numberOfLines = 3
        return label
    }()
    
    lazy var featureLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var timeStampLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(typeLabel)
        addSubview(urlLabel)
        addSubview(messageLabel)
        addSubview(timeStampLabel)
        addSubview(featureLabel)
        
        typeLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = typeLabel.superview else { return }
            make.top.equalTo(superview).offset(8)
            make.leading.equalTo(superview).offset(8)
            make.height.equalTo(20)
        }
        
        urlLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = urlLabel.superview else { return }
            make.top.equalTo(typeLabel.snp.bottom).offset(4)
            make.leading.equalTo(typeLabel)
            make.trailing.equalTo(superview).offset(-8)
        }
        
        timeStampLabel.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(typeLabel.snp.right).offset(8)
            make.centerY.equalTo(typeLabel)
        }
        
        featureLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = featureLabel.superview else { return }
            make.trailing.equalTo(superview).offset(-8)
            make.centerY.equalTo(typeLabel)
        }
        
        messageLabel.snp.makeConstraints { (make) -> Void in
            guard let superview = messageLabel.superview else { return }
            make.top.equalTo(urlLabel.snp.bottom).offset(8)
            make.trailing.equalTo(superview).offset(-8)
            make.bottom.equalTo(superview).offset(-8)
            make.leading.equalTo(typeLabel).offset(8)
        }
    }
    
    public func configure(logItem: OokbeeNetworkLogDescriptor) {
        let statusCode = logItem.status ?? 0
        let statusColor = (200...299).contains(statusCode) ? OokbeeLog.LogType.debug.toColor : OokbeeLog.LogType.error.toColor
        self.typeLabel.text = logItem.type == .request ? "Request" : "Status \(String(statusCode))"
        self.typeLabel.backgroundColor = logItem.type == .request ? logItem.type.toColor : statusColor
        
        self.urlLabel.text = logItem.url?.absoluteString ?? "-"
        
        self.featureLabel.text = logItem.feature
        
        self.messageLabel.text = logItem.message
        self.messageLabel.textColor = logItem.type.toMessageColor
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "d MMM yy - HH:mm:ss"
        
        self.timeStampLabel.text = formatter.string(from: logItem.timestamp)
    }

}
