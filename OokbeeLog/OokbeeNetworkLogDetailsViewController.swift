//
//  OokbeeNetworkLogDetailsViewController.swift
//  OokbeeLog
//
//  Created by Peerasak Unsakon on 15/7/2564 BE.
//

import UIKit
import SnapKit
import Highlightr

class OokbeeNetworkLogDetailsViewController: UIViewController {

    lazy var typeLabel: OokbeeLogTypeLabel = {
        let label = OokbeeLogTypeLabel(withInsets: 8, 8, 8, 8)
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .black
        label.textAlignment = .center
        label.backgroundColor = .red
        label.numberOfLines = 1
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
        label.sizeToFit()
        return label
    }()
    
    lazy var featureLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var timeStampLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var urlLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .gray
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    lazy var textView: UITextView = {
        var textView = UITextView()
        textView.isEditable = false
        textView.layer.cornerRadius = 8
        textView.backgroundColor = UIColor(red: 0.05, green: 0.07, blue: 0.09, alpha: 1.00)
        return textView
    }()
    
    private var logItem: OokbeeNetworkLogDescriptor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupSubviews()
        self.setupViewConstraints()
        self.setupLabelContent()
        self.setupTextViewContent()
        
        self.view.backgroundColor = .white
    }
    
    public func contentConfigure(logItem: OokbeeNetworkLogDescriptor) {
        self.logItem = logItem
    }
    
    private func setupSubviews() {
        self.view.addSubview(self.typeLabel)
        self.view.addSubview(self.timeStampLabel)
        self.view.addSubview(self.featureLabel)
        self.view.addSubview(self.urlLabel)
        self.view.addSubview(self.textView)
    }
    
    private func setupNavigationBar() {
        self.title = "Details"
        
        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareButtonTapped(sender:)))
        
        self.navigationItem.rightBarButtonItems = [shareButton]
    }
    
    private func setupViewConstraints() {
        
        self.typeLabel.snp.makeConstraints { make in
            make.top.equalTo(self.view).offset(68)
            make.leading.equalTo(self.view).offset(8)
            make.height.equalTo(20)
        }
        
        self.timeStampLabel.snp.makeConstraints { make in
            make.centerY.equalTo(self.typeLabel)
            make.leading.equalTo(self.typeLabel.snp.trailing).offset(4)
        }

        self.featureLabel.snp.makeConstraints { make in
            make.centerY.equalTo(self.typeLabel)
            make.trailing.equalTo(self.view).offset(-8)
        }

        self.urlLabel.snp.makeConstraints { make in
            make.top.equalTo(self.typeLabel.snp.bottom).offset(4)
            make.leading.equalTo(self.typeLabel)
            make.trailing.equalTo(self.view).offset(-8)
        }
        
        self.textView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.urlLabel.snp.bottom).offset(8)
            make.trailing.equalTo(self.view).offset(-8)
            make.bottom.equalTo(self.view).offset(-8)
            make.leading.equalTo(self.view).offset(8)
        }
    }
    
    private func setupLabelContent() {
        guard let logItem = self.logItem else { return }
        
        let statusCode = logItem.status ?? 0
        let statusColor = (200...299).contains(statusCode) ? OokbeeLog.LogType.debug.toColor : OokbeeLog.LogType.error.toColor
        if statusCode == 0 {
            self.typeLabel.text = "Request"
            self.typeLabel.backgroundColor = OokbeeLog.LogType.request.toColor
        }else {
            self.typeLabel.text = "Status \(String(statusCode))"
            self.typeLabel.backgroundColor = statusColor
        }
        
        self.urlLabel.text = logItem.url?.absoluteString ?? "-"

        self.featureLabel.text = logItem.feature

        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.dateFormat = "d MMM yy - HH:mm:ss"

        self.timeStampLabel.text = formatter.string(from: logItem.timestamp)
        
    }
    
    private func setupTextViewContent() {
        guard let logItem = self.logItem else { return }
        let statusCode = logItem.status ?? 0
        
        if statusCode == 0 {
            
        }else {
            guard
                let highlightr = Highlightr(),
                let highlightedCode = highlightr.highlight(logItem.prettyPrintMessage, as: "json")
            else {
                return
            }
            highlightr.setTheme(to: "github-dark")
            self.textView.attributedText = highlightedCode
            self.textView.backgroundColor = highlightr.theme.themeBackgroundColor
        }
    }
    
    @objc func shareButtonTapped(sender: Any?) {
        
        guard let logItem = self.logItem else { return }
        
        var message: String = ""
        
        if let url = logItem.url {
            message = "\(url)\n\n"
        }
        
        message = message + logItem.prettyPrintMessage
        
        let activityViewController = UIActivityViewController(activityItems: [message], applicationActivities: nil)
        
        self.present(activityViewController, animated: true) {
            
        }
        
        if let popOver = activityViewController.popoverPresentationController {
          popOver.sourceView = self.view
        }
    }
    
}
