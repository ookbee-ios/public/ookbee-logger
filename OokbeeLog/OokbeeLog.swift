//
//  OokbeeLog.swift
//  OokbeeLog
//
//  Created by Peerasak Unsakon on 8/6/2564 BE.
//

import Foundation
import UIKit

public protocol OokbeeLogDescriptor {
    var type: OokbeeLog.LogType { get }
    var feature: String { get }
    var timestamp: Date { get }
    var message: String { get }
}

public protocol OokbeeNetworkLogDescriptor: OokbeeLogDescriptor {
    var url: URL? { get }
    var status: Int? { get }
    var prettyPrintMessage: String { get }
}

public struct OokbeeLogItem: OokbeeLogDescriptor, Codable {
    public var type: OokbeeLog.LogType
    public var feature: String = "global"
    public var timestamp: Date = Date()
    public var message: String
}

public struct OokbeeNetworkLogItem: OokbeeNetworkLogDescriptor, Codable {
    public var type: OokbeeLog.LogType
    public var feature: String = "network"
    public var timestamp: Date = Date()
    public var message: String
    public var url: URL?
    public var status: Int?
    
    public var prettyPrintMessage: String {
        guard let prettyPrint = message.data(using: .utf8)?.prettyPrintedJSONString as String? else {
            return message
        }
        return prettyPrint
    }
}

public final class OokbeeLogService {
    
    func push(logs: [OokbeeLogItem], completion: @escaping (Bool) -> Void, reject: @escaping (Error) -> Void) {
        let url = URL(string: "https://script.google.com/macros/s/\(OokbeeLog.shared.serviceId)/exec")!
        var request = URLRequest(url: url)
        request.setValue(
            "application/json",
            forHTTPHeaderField: "Content-Type"
        )
        
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let jsonData = try JSONEncoder().encode(logs)
                request.httpMethod = "POST"
                request.httpBody = jsonData
            } catch {
                OokbeeLog.error(text: error.localizedDescription)
            }
            
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                
                if let error = error {
                    reject(error)
                } else if let _ = data {
                    completion(true)
                } else {
                    completion(false)
                }
            }
            
            task.resume()
        }
    }
}

public final class OokbeeLog {
    
    public enum LogType: String, Codable {
        case debug = "debug"
        case info = "info"
        case warning = "warning"
        case error = "error"
        case request = "request"
        case response = "response"
        
        var toString: String {
            switch self {
            case .debug:
                return "DBL"
            case .error:
                return "ERL"
            case .info:
                return "IFL"
            case .warning:
                return "WNL"
            case .request:
                return "REQ"
            case .response:
                return "RES"
            }
        }
        
        var toTitle: String {
            return self.rawValue.uppercased()
        }
        
        var toColor: UIColor {
            switch self {
            case .debug:
                return UIColor(red: 0.62, green: 0.98, blue: 0.76, alpha: 1.00)
            case .error:
                return UIColor(red: 0.92, green: 0.59, blue: 0.58, alpha: 1.00)
            case .info:
                return UIColor(red: 0.89, green: 0.94, blue: 0.95, alpha: 1.00)
            case .warning:
                return UIColor(red: 0.98, green: 0.82, blue: 0.76, alpha: 1.00)
            case .request:
                return UIColor(red: 0.83, green: 0.77, blue: 0.98, alpha: 1.00)
            case .response:
                return UIColor(red: 0.83, green: 0.77, blue: 0.98, alpha: 1.00)
            }
        }
        
        var toMessageColor: UIColor {
            switch self {
            case .debug:
                return UIColor(red: 0.19, green: 0.20, blue: 0.26, alpha: 1.00)
            case .error:
                return UIColor(red: 0.86, green: 0.27, blue: 0.42, alpha: 1.00)
            case .info:
                return UIColor(red: 0.19, green: 0.20, blue: 0.26, alpha: 1.00)
            case .warning:
                return UIColor(red: 0.99, green: 0.24, blue: 0.24, alpha: 1.00)
            case .request:
                return UIColor(red: 0.00, green: 0.34, blue: 0.54, alpha: 1.00)
            case .response:
                return UIColor(red: 0.00, green: 0.34, blue: 0.54, alpha: 1.00)
            }
        }
        
    }
    
    public static let shared: OokbeeLog = {
        let instance = OokbeeLog()
        return instance
    }()
    
    public var serviceId: String = ""
    
    private var store: [OokbeeLogDescriptor] = []
    
    private var maximumRecords: Int = 20
    
    public func setupMaximumRecord(to amount: Int) {
        self.maximumRecords = amount
    }
    
    private func saveLog(logItem: OokbeeLogDescriptor) {
        guard
            !self.store.isEmpty,
            self.store.count >= self.maximumRecords
        else {
            self.store.append(logItem)
            return
        }
        
        self.store.removeFirst()
    }
    
    private func log(type: LogType, feature: String, msg: String, save: Bool) {
        debugPrint("[APPLOG:\(type.toString) - \(Date())] - \(msg)")
        if save {
            let log = OokbeeLogItem(type: type, feature: feature, timestamp: Date(), message: msg)
            self.saveLog(logItem: log)
        }
    }
    
    private func log(_ request: URLRequest) {
        guard let url = request.url else {
            return
        }
        debugPrint("[APPLOG:\(LogType.request.toString) - \(Date())] - \(request.curlString)")
        let log = OokbeeNetworkLogItem(type: .request, message: request.curlString, url: url, status: nil)
        self.saveLog(logItem: log)
    }
    
    private func log(data: Data?, response: URLResponse?, error: Error?) {
        
        guard
            let response = response as? HTTPURLResponse
        else {
            debugPrint("[APPLOG:\(LogType.response.toString) - \(Date())] - Invalid Response")
            let log = OokbeeNetworkLogItem(type: .response, message: "Invalid Response", url: response?.url, status: nil)
            self.saveLog(logItem: log)
            return
        }
        
        if let err = error {
            debugPrint("[APPLOG:\(LogType.response.toString) - \(Date())] - \(err.localizedDescription)")
            let log = OokbeeNetworkLogItem(type: .response, message: err.localizedDescription, url: response.url, status: response.statusCode)
            self.saveLog(logItem: log)
            return
        }
        
        let message = String(decoding: data ?? Data("".utf8), as: UTF8.self)
        debugPrint("[APPLOG:\(LogType.response.toString) - \(Date())] - \(message)")
        let log = OokbeeNetworkLogItem(type: .response, message: message, url: response.url, status: response.statusCode)
        self.saveLog(logItem: log)        
    }
    
    public static func numberOfRows(feature: String? = nil) -> Int {
        guard
            let feature = feature else {
            return OokbeeLog.shared.store.count
        }
        return OokbeeLog.shared.store.filter({ (item) -> Bool in
            item.feature == feature
        }).count
    }
    
    public static func getLogs(feature: String? = nil) -> [OokbeeLogDescriptor] {
        guard
            let feature = feature else {
            return OokbeeLog.shared.store
        }
        return OokbeeLog.shared.store.filter({ (item) -> Bool in
            item.feature == feature
        })
    }
    
    public static func clearLogs() {
        OokbeeLog.shared.store.removeAll()
    }
    
    public static func configure(key: String) {
        OokbeeLog.shared.serviceId = key
    }
    
    public static func setMaximumRecord(to amount: Int) {
        OokbeeLog.shared.setupMaximumRecord(to: amount)
    }
    
    public static func logItem(at indexPath: IndexPath, feature: String? = nil) -> OokbeeLogDescriptor {
        let sortedLogs = OokbeeLog.shared.store.sorted { itemA, itemB in
            return itemA.timestamp.compare(itemB.timestamp) == .orderedAscending
        }
        
        guard
            let feature = feature else {
            return sortedLogs[indexPath.row]
        }
        
        let filteredLogs = sortedLogs.filter({ (item) -> Bool in
            item.feature == feature
        })
        
        return filteredLogs[indexPath.row]
    }
    
    public static func request(_ request: URLRequest) {
        OokbeeLog.shared.log(request)
    }
    
    public static func response(_ data: Data?, response: URLResponse?, error: Error?) {
        OokbeeLog.shared.log(data: data, response: response, error: error)
    }
    
    public static func debug(text: String, feature: String = "global", save: Bool = false) {
        OokbeeLog.shared.log(type: .debug, feature: feature, msg: text, save: save)
    }
    
    public static func info(text: String, feature: String = "global", save: Bool = false) {
        OokbeeLog.shared.log(type: .info, feature: feature, msg: text, save: save)
    }
    
    public static func warning(text: String, feature: String = "global", save: Bool = false) {
        OokbeeLog.shared.log(type: .warning, feature: feature, msg: text, save: save)
    }
    
    public static func error(text: String, feature: String = "global", save: Bool = false) {
        OokbeeLog.shared.log(type: .error, feature: feature, msg: text, save: save)
    }
    
}
